from time import time,sleep
import subprocess
import signal


class GracefulKiller:
  kill_now = False
  def __init__(self):
    signal.signal(signal.SIGINT, self.exit_gracefully)
    signal.signal(signal.SIGTERM, self.exit_gracefully)

  def exit_gracefully(self, signum, frame):
    self.kill_now = True


if __name__ == '__main__':
  print("Fan logger for Dell server")
  killer = GracefulKiller()

  fname = "/root/fan_log.csv"

  with open(fname,"w") as f:
    pass

  while not killer.kill_now:
    reading = subprocess.check_output("/opt/dell/srvadmin/bin/omreport chassis fans | grep Reading", shell="True")
    values = [str(int(s)) for s in reading.split() if s.isdigit()]
    with open(fname,"a") as f:
      f.write(str(time()))
      f.write(";")
      f.write(";".join(values))
      f.write("\n")
    sleep(1)


  print("Exiting fan logger")
