from getpass import getpass
import matplotlib.pyplot as plt
import numpy as np
import requests
from io import BytesIO


password = getpass()
g5kauth = ("afayebedrin",password)

def get_job_details(job):
    url = "https://api.grid5000.fr/3.0/sites/%s/jobs/%d"%(*job,)
    return requests.get(url, auth=g5kauth).json()

def req_file(filename,job):
    return requests.get("https://api.grid5000.fr/sid/sites/%s/public/afayebedrin/job_%s/%s"%(*job,filename), auth=g5kauth)

def npload(file,job):
    return np.load(BytesIO(req_file(file,job).content))

def get_node_power(job,node):
    return npload("%s.npy"%node,job)

def get_timestamps(job):
    return npload("timestamps.npy",job)

def main(*args):
    while True:
        site = input("Site : ")
        if site=="exit":
            return
        job_id = int(input("Job : "))
        job = (site,job_id)
        
        job_details = get_job_details(job)
        
        timestamps = get_timestamps(job)
        nodes = job_details["assigned_nodes"]
        print("Job nodes : ", nodes)
        power = dict()
        for node in nodes:
            node_name = node.split(".")[0]
            print("Getting %s power"%node_name)
            power[node_name] = get_node_power(job,node_name)
            #plt.figure(node_name)
            plt.plot(power[node_name][1],power[node_name][0],label="%s power"%node_name)
        plt.legend()
        plt.show()
    

if __name__ == "__main__":
    main()
