from time import time, sleep
from get_power import get_power
from sys import argv
import execo_g5k as ek
from os import system, mkdir
import matplotlib.pyplot as plt
import numpy as np

def sub(n,cluster):
	job = ek.oarsub([(ek.OarSubmission("cluster=1/nodes=%d"%n, walltime=3600, job_type="deploy", sql_properties="cluster='%s' AND wattmeter='YES'"%cluster), None)])
	#job = ek.oarsub([(ek.OarSubmission("cluster=1/nodes=%d"%n, walltime=3600, job_type="deploy", sql_properties="cluster not in ('nova','pyxis') AND wattmeter='YES'"), None)])
	#job = ek.oarsub([(ek.OarSubmission("cluster=1/nodes=%d"%n, walltime=3600, job_type="deploy", sql_properties="cluster not in ('nova','pyxis','sagittaire') AND wattmeter='YES'"), None)])
	job_id, site = job[0]
	nodes = ek.get_oar_job_nodes(job_id, site)
	return (job_id,nodes,site)

def deploy(nodes):
	#system("kadeploy3 -m {} -e debian10-x64-min -k".format(node.address))
	deployment = ek.kadeploy.Deployment(hosts=nodes, env_name="debian10-x64-min")
	print("Deploying {}".format(deployment.env_name))
	un_deployed_hosts = ek.kadeploy.deploy(deployment)
	print("Deployed hosts :",*(un_deployed_hosts[0]))
	print("Not deployed hosts :",*(un_deployed_hosts[1]))
	return un_deployed_hosts[0]

def file_nodes(data_dir,dnodes):
	fname = "%s/deployed_nodes.list"%data_dir
	with open(fname,"w") as f:
		f.write("\n".join(dnodes))
	return fname

def end_job(job_id,site):
	ek.oardel([(job_id, site)])

def poweron(fnodes):
	system("kapower3 -f {} --on --wait".format(fnodes))

def poweroff(fnodes):
	system("kapower3 -f {} --off --wait".format(fnodes))

def _mark(t):
	plt.plot([t,t],[0,200])

def retrieve_and_plot(node_name,title,start,stop):
	power = get_power(node_name,start,stop)
	plt.figure()
	plt.plot(power[1],power[0])
	_mark(start)
	_mark(stop)
	plt.savefig("/home/afayebedrin/public/"+title+".png")
	return power

def sub_and_deploy(n,cluster):
	print("Asking for job")
	job_id,nodes,site = sub(n,cluster)
	print("Job {} on hosts {}".format(job_id,nodes))
	data_dir = "/home/afayebedrin/public/job_%d"%job_id
	mkdir(data_dir)

	print("Deploying nodes")
	start_deploy_time = time()
	dnodes = deploy(nodes)
	fnodes = file_nodes(data_dir,dnodes)
	end_deploy_time = time()
	print("Machines deployed")

	print("Waiting before power off")
	sleep(30)

	print("Powering off machines")
	start_poweroff_time = time()
	poweroff(fnodes)
	end_poweroff_time = time()
	print("Machines off")

	print("Waiting before power on")
	sleep(10)

	print("Powering on machines")
	start_poweron_time = time()
	poweron(fnodes)
	end_poweron_time = time()
	print("Machines on")

	sleep(240)

	print("Deleting job")
	end_job(job_id,site)
	print("Job deleted")
	last_time = time()


	print("Retrieving power from shutdown to power up")

	for node in dnodes:
		node_name = node.split('.')[0]
		print("...for %s"%node_name)
		power_total = get_power(node_name, start_poweroff_time-10, last_time)
		plt.figure()
		plt.plot(power_total[1],power_total[0])
		_mark(start_poweroff_time)
		_mark(start_poweron_time)
		plt.savefig("%s/%s.png"%(data_dir,node_name))
		np.save("%s/%s"%(data_dir,node_name), power_total)
	np.save("%s/timestamps"%(data_dir),np.array([start_poweroff_time,end_poweroff_time,start_poweron_time,end_poweron_time]))


def main(*args):
	n=5
	if(len(args)):
		n = int(args[0])
	cluster="taurus"
	if(len(args)>1):
		cluster=args[1]
	sub_and_deploy(n,cluster)


if __name__ == "__main__":
	main(*argv[1:])
