from time import time, sleep
from datetime import datetime
from get_power import get_power
from sys import argv
import execo_g5k as ek
from os import system,mkdir
import matplotlib.pyplot as plt
import numpy as np

def sub():
	job = ek.oarsub([(ek.OarSubmission(walltime=3600, job_type="deploy", sql_properties="cluster not in ('nova','pyxis') AND wattmeter='YES'"), None)])
	# parapide et parapluie n'ont pas une résolution temporelle intéressante, paranoia n'est pas monitoré
	#job = ek.oarsub([(ek.OarSubmission(walltime=600, job_type="deploy"), None)])
	job_id, site = job[0]
	node = ek.get_oar_job_nodes(job_id, site)[0]
	return (job_id,node,site)

def deploy(node):
	system("kadeploy3 -m {} -e debian10-x64-min -k".format(node.address))
	"""deployment = ek.kadeploy.Deployment(hosts=[node], env_name="debian10-x64-min")
	print("Deploying {}".format(deployment.env_name))
	#deployer = ek.kadeploy.Kadeployer(deployment)
	un_deployed_hosts = ek.kadeploy.deploy(deployment)
	print("Deployed hosts :",*(un_deployed_hosts[0]))
	print("Not deployed hosts :",*(un_deployed_hosts[1]))"""

def end_job(job_id,site):
	ek.oardel([(job_id, site)])

def poweroff(node):
	system("kapower3 -m {} --off --wait".format(node.address))

def poweron(node):
	system("kapower3 -m {} --on --wait".format(node.address))

def powerstatus(node):
	system("kapower3 -m {} --status".format(node.address))

def _mark(t):
	plt.plot([t,t],[0,200])

def ssh_command(node,cmd):
	system("bash -c 'ssh root@{} {}'".format(node.address,cmd))

def install_file(src, dest, node):
	local_dir = "/home/afayebedrin/pyexs/to_install/"
	system("scp {}{} root@{}:{}".format(local_dir,src,node.address,dest))

def retrieve_file(file, dest, node):
	system("scp root@{}:{} {}".format(node.address,file,dest))

def get_booted_timestamp(node,service="sshd"):
	ssh_command(node,"systemctl show {} --value -p ActiveEnterTimestamp > booted.timestamp".format(service))
	with open("booted.timestamp") as bt:
		line=bt.readline()[:-1]
		print(line)
		return datetime.strptime(line,"%a %Y-%m-%d %H:%M:%S %Z").timestamp()

def get_node_time(node):
	ssh_command(node,"date +%s > node.timestamp")
	with open("node.timestamp") as nt:
		return float(int(nt.readline()))

def sub_and_deploy():
	print("Asking for job")
	job_id,node,site = sub()
	print("Job {} on host {}".format(job_id,node.address))
	node_name = node.address.split('.')[0]
	data_dir = "/home/afayebedrin/public/job_%d"%job_id
	mkdir(data_dir)


	print("Deploying node")
	start_deploy_time = time()
	deploy(node)
	end_deploy_time = time()
	print("Machine deployed")

	packages=["apache2","dstat","python3-pip"];
	ssh_command(node,"apt update")
	print("Installing packages : {}".format(", ".join(packages)))
	ssh_command(node," ".join(("apt install -y",*packages)))

	pip_packages=["pyghmi"]
	print("Installing Python3 modules from PyPI : {}".format(", ".join(pip_packages)))
	ssh_command(node," ".join(("pip3 install",*pip_packages)))

	print("Installing dstat service")
	install_file("dstat.service", "/etc/systemd/system/", node)
	print("Installing fan log service")
	install_file("fans-log.service", "/etc/systemd/system/fans-log.service", node)
	install_file("fans-log.py", "/root/", node)
	print("Enablig services")
	ssh_command(node, "systemctl daemon-reload")
	ssh_command(node, "systemctl enable dstat fans-log")


	print("Waiting before power off")
	sleep(30)


	print("Powering off machine")
	start_poweroff_time = time()
	poweroff(node)
	end_poweroff_time = time()
	print("Machine off")


	print("Waiting before power on")
	sleep(10)


	print("Powering on machine")
	start_poweron_time = time()
	poweron(node)
	end_poweron_time = time()
	print("Machine on")


	sleep(240)


	print("Retrieving startup load")
	retrieve_file("/root/dstat.csv", "%s/dstat.csv"%data_dir, node)

	print("Retrieving fan speed")
	retrieve_file("/root/fan_log.csv", "%s/fan_log.csv"%data_dir, node)

	print("Retrieving temperatures")
	retrieve_file("/root/temp_log.csv", "%s/temp_log.csv"%data_dir, node)

	print("Retrieving boot chart")
	ssh_command(node, "systemd-analyze plot > %s/bootchart.svg"%data_dir)


	#print("Boot time")
	#ssh_command(node,"systemd-analyze time")

	print("SSH start timestamp")
	sshd_time = get_booted_timestamp(node,service="sshd")
	print(sshd_time)

	print("Apache2 start timestamp")
	apache2_time = get_booted_timestamp(node,service="apache2")
	print(apache2_time)

	print("Getty start timestamp")
	getty_time = get_booted_timestamp(node,service="getty@tty1")
	print(getty_time)


	print("Time difference between node and master :")
	node_time = get_node_time(node)
	local_time = time()
	time_difference = node_time - local_time
	print("{} - {} = {}".format(node_time,local_time,time_difference))


	print("Deleting job")
	end_job(job_id,site)
	print("Job deleted")
	last_time = time()

	print("Retrieving power during deployment")
	power_depl = get_power(node_name, start_deploy_time, end_deploy_time)
	plt.plot(power_depl[1],power_depl[0])
	plt.savefig("%s/depl.png"%data_dir)

	print("Retrieving power during shutdown")
	power_shutdown = get_power(node_name, start_poweroff_time, end_poweroff_time+5)
	plt.figure()
	plt.plot(power_shutdown[1],power_shutdown[0])
	_mark(start_poweroff_time)
	_mark(end_poweroff_time)
	plt.savefig("%s/shutdown.png"%data_dir)

	print("Retrieving power during boot")
	power_boot = get_power(node_name, start_poweron_time, end_poweron_time)
	plt.figure()
	plt.plot(power_boot[1],power_boot[0])
	_mark(start_poweron_time)
	_mark(end_poweron_time)
	plt.savefig("%s/boot.png"%data_dir)

	print("Retrieving power from shutdown to power up")
	power_total = get_power(node_name, start_poweroff_time-10, last_time)
	plt.figure()
	plt.plot(power_total[1],power_total[0])
	_mark(start_poweroff_time)
	_mark(start_poweron_time)
	_mark(apache2_time)
	_mark(sshd_time)
	_mark(getty_time)
	plt.savefig("%s/total.png"%data_dir)


	np.save("%s/power"%(data_dir), power_total)
	np.save("%s/timestamps"%(data_dir),np.array([start_poweroff_time,end_poweroff_time,start_poweron_time,end_poweron_time,apache2_time,sshd_time,getty_time]))



def main(*args):
	sub_and_deploy()


if __name__ == "__main__":
	main(argv[1:])
