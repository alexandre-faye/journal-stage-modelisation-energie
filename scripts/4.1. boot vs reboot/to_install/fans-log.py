from time import time,sleep
import subprocess
import signal
from pyghmi.ipmi.command import Command

class GracefulKiller:
  kill_now = False
  def __init__(self):
    signal.signal(signal.SIGINT, self.exit_gracefully)
    signal.signal(signal.SIGTERM, self.exit_gracefully)

  def exit_gracefully(self, signum, frame):
    self.kill_now = True


if __name__ == '__main__':
  print("Fan and temperature logger for IPMI-compatible server")
  killer = GracefulKiller()

  fan_fname = "/root/fan_log.csv"
  temp_fname = "/root/temp_log.csv"
  power_fname = "/root/power_log.csv"

  with open(fan_fname,"w") as fan_f:
    with open(temp_fname,"w") as temp_f:
      with open(power_fname,"w") as power_f:

        ipmi=Command()

        fan_f.write("timestamp")
        temp_f.write("timestamp")
        power_f.write("timestamp")

        sensors = list(ipmi.get_sensor_data())
        #print(*sensors,sep="\n")

        for sensor in sensors:
          if sensor.value is not None:
            # correction d'un problème apparu sur Sagittaire
            sname=sensor.name.strip('\x00')
            if sensor.type=="Fan":
              fan_f.write(";%s"%sname)
            if sensor.type=="Temperature":
              temp_f.write(";%s"%sname)
            if sensor.units=="W":
              power_f.write(";%s"%sname)

        fan_f.write("\n{}".format(time()))
        temp_f.write("\n{}".format(time()))
        power_f.write("\n{}".format(time()))
        for sensor in sensors:
          if sensor.value is not None:
            if sensor.type=="Fan":
              fan_f.write(";%s"%sensor.value)
            if sensor.type=="Temperature":
              temp_f.write(";%s"%sensor.value)
            if sensor.units=="W":
              power_f.write(";%s"%sensor.value)
        power_f.write("\n")
        fan_f.write("\n")
        temp_f.write("\n")
        power_f.flush()
        fan_f.flush()
        temp_f.flush()

        while not killer.kill_now:
          sleep(6)
          fan_f.write("{}".format(time()))
          temp_f.write("{}".format(time()))
          power_f.write("{}".format(time()))
          for sensor in ipmi.get_sensor_data():
            if sensor.value is not None:
              if sensor.type=="Fan":
                fan_f.write(";%s"%sensor.value)
              if sensor.type=="Temperature":
                temp_f.write(";%s"%sensor.value)
              if sensor.units=="W":
                power_f.write(";%s"%sensor.value)
          power_f.write("\n")
          fan_f.write("\n")
          temp_f.write("\n")
          power_f.flush()
          fan_f.flush()
          temp_f.flush()



  print("Exiting fan and temperature logger")
