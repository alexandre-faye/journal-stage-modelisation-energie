import requests
from numpy import array
import time
import datetime
import bz2
from execo_g5k import get_host_attributes


def get_power(node, start, stop):
    watt = ([],[])
    node_wattmetre = get_host_attributes(node)['sensors']['power']['via']['pdu'][0]
    for ts in range(int(start), int(stop)+3600, 3600):
        suffix = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%dT%H')
        if suffix != datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%dT%H'):
            suffix += ".bz2"
        data=requests.get("http://wattmetre.lyon.grid5000.fr/data/"+node_wattmetre['uid']+"-log/power.csv."+suffix).content
        if suffix.endswith(".bz2"):
            data = bz2.decompress(data)
        for l in str(data).split('\\n')[1:-1]:
            l = l.split(',')
            if l[3] == 'OK' and l[4+node_wattmetre['port']] != '':
                ts, value = (float(l[2]), float(l[4+node_wattmetre['port']]))
                if start <= ts and ts <= stop:
                    watt[0].append(value)
                    watt[1].append(ts)
        if not suffix.endswith(".bz2"):
            break
    return watt
