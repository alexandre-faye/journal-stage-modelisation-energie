from time import time, sleep
from datetime import datetime
from get_power import get_power
from sys import argv
import execo_g5k as ek
from os import system,mkdir
import matplotlib.pyplot as plt
import numpy as np
import threading
import json


nodes_number = 4 
experiments_number = 5
cluster_name = "orion"


def sub():
	job = ek.oarsub([(ek.OarSubmission(resources="/nodes=%i"%nodes_number, walltime=3600, job_type="deploy", sql_properties="cluster='%s' AND wattmeter='YES'"%cluster_name), None)])
	job_id, site = job[0]
	nodes = ek.get_oar_job_nodes(job_id, site)
	return (job_id,nodes,site)

def deploy(node):
	system("kadeploy3 -m {} -e debian10-x64-min -k".format(node.address))

def end_job(job_id,site):
	ek.oardel([(job_id, site)])

def poweroff(node):
	system("kapower3 -m {} --off --wait".format(node.address))

def poweron(node):
	system("kapower3 -m {} --on --wait".format(node.address))

def powerstatus(node):
	system("kapower3 -m {} --status".format(node.address))

def _mark(t):
	plt.plot([t,t],[0,200])

def ssh_command(node,cmd):
	system("bash -c 'ssh root@{} {}'".format(node.address,cmd))

def install_file(src, dest, node):
	local_dir = "/home/afayebedrin/pyexs/to_install/"
	system("scp {}{} root@{}:{}".format(local_dir,src,node.address,dest))

def retrieve_file(file, dest, node):
	system("scp root@{}:{} {}".format(node.address,file,dest))

def average_power(power, start_time, end_time):
	power=np.array(power)
	cond = (power[1]>=start_time)*(power[1]<end_time)
	return np.average(power[0][cond])

def energy(power, start_time, end_time):
	power=np.array(power)
	cond = (power[1]>=start_time)*(power[1]<end_time)
	time = power[1][cond]
	power = power[0][cond]
	return np.sum(power)*(time[-1]-time[0])/len(time)

boot_lock = threading.Lock()
def get_booted_timestamp(node,service="sshd"):
	boot_lock.acquire()
	ssh_command(node,"systemctl show {} --value -p ActiveEnterTimestamp > booted.timestamp".format(service))
	with open("booted.timestamp") as bt:
		line=bt.readline()[:-1]
		print(line)
		boot_lock.release()
		return datetime.strptime(line,"%a %Y-%m-%d %H:%M:%S %Z").timestamp()

time_lock = threading.Lock()
def get_node_time(node):
	time_lock.acquire()
	ssh_command(node,"date +%s > node.timestamp")
	with open("node.timestamp") as nt:
		line=nt.readline()
		time_lock.release()
		return float(int(line))

def sub_and_deploy():
	print("Asking for job")
	job_id,nodes,site = sub()
	print("Job {} on hosts {}".format(job_id,", ".join([node.address for node in nodes])))
	data_dir = "/home/afayebedrin/public/job_%d"%job_id
	mkdir(data_dir)

	data_store=dict()

	threads=[]
	for node in nodes:
		data_store[node.address]=dict()
		thread=threading.Thread(target=deploy_and_experiment, args=(node,data_dir,job_id,data_store[node.address]))
		thread.start()
		threads.append(thread)
	for thread in threads:
		thread.join()

	print("Deleting job")
	end_job(job_id,site)
	print("Job deleted")


	data_store["meta"]={
		"job_id":job_id,
		"time":time(),
		"site":site,
		"cluster":cluster_name,
	}

	values_lists={
                "boot_time":[],
                "shutdown_time":[],
                "idle_power":[],
                "off_power":[],
                "boot_energy":[],
                "shutdown_energy":[],
		"threshold_time":[]
	}
	for node in nodes:
		ds=data_store[node.address]
		for exp in ds:
			cvl=ds[exp]["computed"]
			for cv in cvl:
				values_lists[cv].append(cvl[cv])

	data_store["computed"]=dict()

	for vl in values_lists:
		data_store["computed"][vl]={
			"avg":np.average(values_lists[vl]),
			"std":np.std(values_lists[vl]),
		}

	print("Saving data")
	with open("%s/data.json"%data_dir,"w") as data_file:
		data_file.write(json.dumps(data_store,indent=4))



def deploy_and_experiment(node,data_dir,job_id,data_store):
	node_name = node.address.split('.')[0]

	print("Deploying node %s"%node_name)
	start_deploy_time = time()
	deploy(node)
	end_deploy_time = time()
	print("Node %s deployed"%node_name)
	sleep(10)

	for exp_id in range(experiments_number):
		data_store[exp_id]=dict()
		experiment(node,data_dir,job_id,exp_id,data_store[exp_id])

def experiment(node,data_dir,job_id,exp_id,data_store):
	node_name = node.address.split('.')[0]

	print("Waiting before power off")
	sleep(20)

	print("Powering off %s"%node_name)
	start_poweroff_time = time()
	poweroff(node)
	end_poweroff_time = time()
	print("%s off"%node_name)

	print("Waiting before power on")
	sleep(20)

	print("Powering on %s"%node_name)
	start_poweron_time = time()
	poweron(node)
	end_poweron_time = time()
	print("Machine %s supposedly on"%node_name)
	print("Waiting before retrieving data")
	sleep(300)

	print("SSH start timestamp on %s"%node_name)
	sshd_time = get_booted_timestamp(node,service="sshd")
	print(sshd_time)

	print("Time difference between node %s and master :"%node_name)
	node_time = get_node_time(node)
	local_time = time()
	time_difference = node_time - local_time
	print("{} - {} = {}s".format(node_time,local_time,time_difference))

	last_time = time()

	print("Retrieving power from shutdown to power up for %s"%node_name)
	power_total = get_power(node_name, start_poweroff_time-10, last_time)
	plt.figure()
	plt.plot(power_total[1],power_total[0])
	_mark(start_poweroff_time)
	_mark(end_poweroff_time)
	_mark(start_poweron_time)
	_mark(sshd_time)
	plt.savefig("%s/power_%s_%s.png"%(data_dir,node_name,exp_id))

	np.save("%s/power_%s_%s"%(data_dir,node_name,exp_id), power_total)
	np.save("%s/timestamps_%s_%s"%(data_dir,node_name,exp_id),np.array([start_poweroff_time,end_poweroff_time,start_poweron_time,end_poweron_time,sshd_time]))

	data_store["start_poweroff_time"]=start_poweroff_time
	data_store["start_poweron_time"]=start_poweron_time
	data_store["end_poweroff_time"]=end_poweroff_time
	data_store["end_poweron_time"]=end_poweron_time
	data_store["sshd_time"]=sshd_time
	data_store["power"]=power_total

	data_store["computed"]={
		"boot_time":sshd_time - start_poweron_time,
		"shutdown_time":end_poweroff_time - start_poweroff_time,
		"idle_power":average_power(power_total, last_time-30, last_time-2),
		"off_power":average_power(power_total, end_poweroff_time+5, start_poweron_time),
		"boot_energy":energy(power_total,start_poweron_time,sshd_time),
		"shutdown_energy":energy(power_total,start_poweroff_time,end_poweroff_time),
	}
	dc=data_store["computed"]
	data_store["computed"]["threshold_time"]=max( (dc["boot_energy"]+dc["shutdown_energy"]-dc["off_power"]*(dc["boot_time"]+dc["shutdown_time"])) / (dc["idle_power"]-dc["off_power"]) ,(dc["boot_time"]+dc["shutdown_time"]))


def main(*args):
	sub_and_deploy()


if __name__ == "__main__":
	main(argv[1:])
